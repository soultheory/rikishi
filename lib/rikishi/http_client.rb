require 'faraday'
require 'faraday_middleware'
require 'faraday-cookie_jar'

module Rikishi
  module HttpClient
    def http_client
      @http_client ||= Faraday.new(url: api_url, headers: headers) do |conn|
        conn.basic_auth(config.access_id, config.access_key)
        conn.use      FaradayMiddleware::FollowRedirects, limit: 5
        conn.use      :cookie_jar
        conn.request  :json
        conn.response :json, content_type: 'application/json'
        conn.adapter  Faraday.default_adapter
      end
    end

    def headers
      {
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
      }
    end

    def api_url
      "https://api#{region_for_url}.sumologic.com/api/v1"
    end

    private

    def region_for_url
      ".#{config.region}" if config.region.present?
    end
  end
end
