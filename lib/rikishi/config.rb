module Rikishi
  class Config
    attr_accessor :access_id, :access_key, :region

    def initialize
      @access_id = nil
      @access_key = nil
      @region = nil
    end
  end
end
