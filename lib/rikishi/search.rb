module Rikishi
  module Search
    def search(query, from_time=nil, to_time=nil, time_zone='UTC')
      resp = http_client.get do |req|
        req.url "logs/search"
        req.body = {
          q: query,
          from: from_time,
          to: to_time,
          tz: time_zone
        }.to_json
      end

      resp.body
    end
  end
end
