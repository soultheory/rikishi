
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "rikishi/version"

Gem::Specification.new do |spec|
  spec.name          = "rikishi"
  spec.version       = Rikishi::VERSION
  spec.authors       = ["Kadeem Pardue"]
  spec.email         = ["kadeem@soultheory.com"]

  spec.summary       = "A ruby wrapper gem that integrates with SumoLogic"
  spec.homepage      = "https://www.soultheory.com"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "faraday", "~> 0.13.1"
  spec.add_dependency "faraday_middleware", "~> 0.12.2"
  spec.add_dependency "faraday-cookie_jar", "~> 0.0.6"
  spec.add_dependency "json", "~> 2.1.0"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry", "~> 0.11.3"
end
