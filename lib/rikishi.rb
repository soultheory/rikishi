require "rikishi/version"
require 'rikishi/config'
require 'rikishi/http_client'
require 'rikishi/search'
require 'json'

# require 'pry'

module Rikishi
  extend Rikishi::HttpClient
  extend Rikishi::Search

  class << self
    attr_accessor :config
  end

  def self.config
    @config ||= Config.new
  end

  def self.reset
    @config = Config.new
  end

  def self.configure
    yield(config)
  end
end
